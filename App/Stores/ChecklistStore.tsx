import { observable, action } from 'mobx';
import Api from '../Libs/Api';

// import AsyncStorage from '@react-native-community/async-storage';
import Storage from '../Libs/Storages' 

class ChecklistStore{

    @observable checklist_data = []





    @action getChecklist(){


        // alert(JSON.stringify('lalal'));
        return Api.get('checklist').then(resp =>{
            // alert(JSON.stringify(resp));
            if(resp.data){
                // alert(JSON.stringify(resp.data));

                this.checklist_data = resp.data
                
            }
            else {
                this.checklist_data = []
            }
        });

    }

    
}

const checklistStore = new ChecklistStore();
export default checklistStore;