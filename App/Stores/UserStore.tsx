import { observable, action } from 'mobx';
import Api from '../Libs/Api';

// import AsyncStorage from '@react-native-community/async-storage';
import Storage from '../Libs/Storages' 

class UserStore{

    @observable user;

    @observable token=null;





    @action login(username, password){

        let params = {
            username: username,
            password: password,
        };

        // alert(JSON.stringify(params));
        return Api.post('login', params).then(resp =>{
            // .consolelog(resp)
            // alert(JSON.stringify(params));
            if(resp.data){

                this.token = resp.data.token
                Storage.setToken(this.token)
                // AsyncStorage.setItem('jwtToken', resp.access_token);
            }
            else {
                this.token = null
            }
        });

    }

    
}

const userStore = new UserStore();
export default userStore;