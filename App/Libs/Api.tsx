import { AsyncStorage } from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios'

import Storage from './Storages'

class Api {

 
    static url = {
        local: 'http://18.141.178.15:8080/',
        
    }

    static host = this.url.local;
    // static host = 'http://192.168.1.156:8080/api/';
    // static host = 'http://172.20.10.2:8080/api/';
    // static host = 'http://192.168.100.7:8181/api/';

    static headers(){

        return{
            'Accept': 'application/json, text/plain, text/json, text/html',
            'Content-Type': 'application/json',
            // 'apikey': 'bb9d3e49a26fe25a66022f9379d93e9b8b400b48',
            // 'dataType': 'json',
            // 'X-Mashape-Key':,
            // 'Authorization' : 'Bearer ' + token,
        }  
     
    }

    static get(route){
        return this.xhr(route, null, 'GET');
    }

    static put(route, params){
        return this.xhr(route, params, 'PUT');
    }

    static post(route, params){
        return this.xhr(route, params, 'POST');
    }

    static delete(route, params){
        return this.xhr(route, params, 'DELETE');
    }

   
      
    static async xhr(route, params, verb){

        const url = this.host + route;
        // const url = `${host}${route}`;

        // alert(JSON.stringify(params));
        let options = Object.assign({method: verb}, params ? {data: JSON.stringify(params)} : null);
        // let options = Object.assign({method: verb}, params ? {body: JSON.stringify(params)} : null, {timeout: 15000});

        options.headers = Api.headers();
        
        
       
        let token = await Storage.getToken()
        // alert(JSON.stringify(token))

        if(token){

            options.headers['Authorization'] = 'Bearer ' + token;
        }
        


        return await axios(url, options)
        .then((response) => {
            // alert(JSON.stringify(response))
            // console.log(response)
            if (response) {
              return response.data
            }
            return null
          })
        .catch((error) => {
            // alert(error)
            return error
        })
       
    }
}

export default Api;