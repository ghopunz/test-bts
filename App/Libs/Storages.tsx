// import AsyncStorage from '@react-native-community/async-storage'
import { AsyncStorage } from 'react-native';

const type = {
  language: 'BTS.LANGUAGE',
  user: 'BTS.USER',
  login: 'BTS.LOGIN',
  token: 'BTS.TOKEN',
  building: 'BTS.BUILDING',
  
}

export default class Storage {

  //BOARDING
  static getBoarding = async () => {
    try {
      const boarding = await AsyncStorage.getItem(type.boarding)
      return boarding
    } catch (error) {
      return error
    }
  }

  static setBoarding = async (boarding) => {
    try {
      const data = await AsyncStorage.setItem(type.boarding, JSON.stringify(boarding))
      return data
    } catch (error) {
      return error
    }
  }

  static removeBoarding = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.boarding)
      return data
    } catch (error) {
      return error
    }
  }

  //LANGUAGE
  static getLanguage = async () => {
    try {
      const language = await AsyncStorage.getItem(type.language)
      return language
    } catch (error) {
      return error
    }
  }

  static setLanguage = async (language) => {
    try {
      const data = await AsyncStorage.setItem(type.language, language)
      return data
    } catch (error) {
      return error
    }
  }

  static removeLanguage = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.language)
      return data
    } catch (error) {
      return error
    }
  }

  //user
  static getUser = async () => {
    try {
      const user = await AsyncStorage.getItem(type.user)
      return JSON.parse(user)
    } catch (error) {
      return error
    }
  }

  static setUser = async (user) => {
    try {
      const data = await AsyncStorage.setItem(type.user, JSON.stringify(user))
      return data
    } catch (error) {
      return error
    }
  }

  static removeUser = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.user)
      return data
    } catch (error) {
      return error
    }
  }

  //Login
  static getLogin = async () => {
    try {
      const login = await AsyncStorage.getItem(type.login)
      return JSON.parse(login)
    } catch (error) {
      return error
    }
  }

  static setLogin = async (login) => {
    try {
      const data = await AsyncStorage.setItem(type.login, login)
      return data
    } catch (error) {
      return error
    }
  }

  static removeLogin = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.login)
      return data
    } catch (error) {
      return error
    }
  }



  //TOKEN LOGIN
  static getToken = async () => {
    try {
      const token = await AsyncStorage.getItem(type.token)
      return token
    } catch (error) {
      return error
    }
  }

  static setToken = async (token) => {
    try {
      const data = await AsyncStorage.setItem(type.token, token)
      return data
    } catch (error) {
      return error
    }
  }

  static removeToken = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.token)
      return data
    } catch (error) {
      return error
    }
  }

  //BUILDING
  static getBuilding = async () => {
    try {
      const building = await AsyncStorage.getItem(type.building)
      return JSON.parse(building)
    } catch (error) {
      return error
    }
  }

  static setBuilding = async (building) => {
    try {
      const data = await AsyncStorage.setItem(type.building, JSON.stringify(building))
      return data
    } catch (error) {
      return error
    }
  }

  static removeBuilding = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.building)
      return data
    } catch (error) {
      return error
    }
  }

 
}
