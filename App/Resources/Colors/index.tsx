/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
    transparent: 'rgba(0,0,0,0)',
    text: '#212529',
    white: '#ffffff',
    primary: '#009CDC',
    success: '#28a745',
    oldBlue: '#243B67',
    mediumBlue: '#1787B0',
    error: '#dc3545',
    red: '#F51212',
    card: '#F2F2F2',
    gray: '#DADADA',
    whiteGray: '#F6F6F6',
    black: '#1e272e',
    darkGray: '#757575',
    green: '#64BB75',
    warning: '#FFCC00',
    orange: '#FEA222',
    purple: '#8499F0',
    lightRed: '#F6BAC2',
  }
  