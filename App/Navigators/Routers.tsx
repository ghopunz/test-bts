
import React from 'react';
import {
  StatusBar,
} from 'react-native';

import {
  Scene,
  Router,

  Stack,
} from 'react-native-router-flux';

import LoginScreen from '../Screens/LoginScreen'
import HomeScreen from '../Screens/HomeScreen'


const Routers = () => {
  return (
    <Router>
      
      <Stack key="root">
        {/* <Scene key="register" component={Register} title="Register" /> */}
        <Scene key="login" component={LoginScreen} title="Login" />
        <Scene key="home" component={HomeScreen} hideNavBar/>
      </Stack>
    </Router>
      
      
  );
};

export default Routers;
