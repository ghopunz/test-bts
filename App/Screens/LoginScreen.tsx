

import React, {Component} from 'react';
import {
  StatusBar, View, Text,
} from 'react-native';

import {
    Container,
    Content,
    Input,
    Button,
} from 'native-base'

import { Actions } from 'react-native-router-flux';

import { inject, observer } from 'mobx-react';

@inject('store')

@observer


export default class LoginScreen extends Component {

    constructor(props) {
		super(props);

		this.state = {
			username:'coba',
            password:'123456',
        }

    }

    login = () =>{

        const { store } = this.props
        const { username, password } = this.state
        
        store.userStore.login(username, password)
        .then(() => {

            if(store.userStore.token){

                Actions.home();
            }
            else{
                alert('Login Failed')
            }

        })
        

    }

    render() {

        

        return (
            <Container
                style={{
                    flex:1,
                    padding: 20,
                }}
            >
                <Content
                    style={{
                     
                    }}
                >
                    
                    <Input 
                        autoCapitalize='none'
                        placeholder={'username'}
                        onChangeText={(text) => this.setState({username:text})}
                    
                    />
                    
                    <Input 
                    
                        autoCapitalize='none'
                        placeholder={'password'}
                        secureTextEntry = { true}
                        onChangeText={(text) => this.setState({password:text})}
                        
                    />
                    
                    <Button
                        info
                        full
                        onPress={() => this.login()}
                    >
                        <Text>

                            Login
                        </Text>
                    </Button>
                </Content>
            </Container>
        );
    }
  }
