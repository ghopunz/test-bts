import React from 'react'
import styles from './AreaItemComponentStyle'
import { Text, TouchableOpacity } from 'react-native'
import { Icon, View } from 'native-base'
import Colors from '../../Resources/Colors'

const ChecklistItemComponent = ({ 
    item,
}) => {

    const {
        name,
      } = item
  return (
    <TouchableOpacity 
        activeOpacity={0.8} 
        // onPress={onPressChecklist}
        style={{
            backgroundColor: Colors.whiteGray,
            marginBottom:10,
            borderRadius: 10,
            height: 100,
            padding:10,

        }}
    >
        

            <View>
                <Text>
                    {name}
                </Text>
            </View>
            
    </TouchableOpacity>
  )
}

export default ChecklistItemComponent
