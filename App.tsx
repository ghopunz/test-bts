

import React from 'react';
import {
  StatusBar,
} from 'react-native';

import {
  Scene,
  Router,

  Stack,
} from 'react-native-router-flux';
import { Provider as MobXProvider} from 'mobx-react';
import  * as Store from './App/Stores';

import Routers from './App/Navigators/Routers'


const App = () => {
  return (
    <MobXProvider store={Store}>
      <Routers />
      {/* <App /> */}
    </MobXProvider>
  );
};

export default App;
